export const DEFAULT_RENDERER_DIR = 'renderer'
export const DEFAULT_EXTENSIONS = ['.mjs', '.cjs', '.js', '.ts', '.jsx', '.tsx', '.node']
export const JS_TYPES_RE = /\.([jt]sx?|[mc]js)$/
export const COMMONJS_REQUIRE = '?commonjs-require'
export const CWD = process.cwd()
