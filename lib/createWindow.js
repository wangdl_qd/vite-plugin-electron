import { BrowserWindow } from 'electron'
import path from 'path'
import { appServer } from './server'

export const createWindow = options => {
  const window = new BrowserWindow({
    ...options,
    show: false,
    webPreferences: {
      ...options?.webPreferences,
      preload: path.join(__dirname, 'preload.js')
    }
  })

  window.maximize()
  window.once('ready-to-show', () => window.show()) // 初始化后再显示

  if (!__DEV__) {
    window.setMenu(null)
  }

  return window
}

let mainWindow

export const createMainWindow = async url => {
  const window = (mainWindow = createWindow())

  window.once('closed', () => {
    if (window === mainWindow) {
      mainWindow = undefined
    }
  })
  window.loadURL(url ?? (await appServer.listen()))

  return window
}
