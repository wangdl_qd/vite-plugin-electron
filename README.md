<h1 align="center">vite-plugin-electron</h1>

<p>
  <img alt="Version" src="https://img.shields.io/badge/version-1.8.0-blue.svg?cacheSeconds=2592000" />
  <img src="https://img.shields.io/badge/node-%3E%3D12.2.0-green.svg" />
  <img src="https://img.shields.io/badge/vite-%3E%3D2.0.0-yellow.svg" />
  <img src="https://img.shields.io/badge/electron-%3E%3D12.0.0-pink.svg" />
  <a href="#" target="_blank">
    <img alt="License: MIT" src="https://img.shields.io/badge/License-MIT-yellow.svg" />
  </a>
</p>

> 一个使用electron-builder构建electron应用程序的vite插件

<quote>
仅需少量配置，即可快速整合vite electron开发环境。
</quote>

## 特性
1. 默认支持模块热替换(方便开发)
2. 默认支持打包静态资源(图片，视频等)
3. 支持.node模块打包
4. 渲染进程支持node模块

## 安装
```bash
npm i --save-dev @xyh19/vite-plugin-electron
# or
yarn add -D @xyh19/vite-plugin-electron
```

## 快速使用
1. 在 `vite.config.js` 文件中注册插件:
```js
import { defineConfig } from 'vite'
import electron from '@xyh19/vite-plugin-electron'

export default defineConfig({
  plugins: [
    electron({
      input: {
        appEntry: 'src/main/app.ts', //app入口
        entries: ['src/main/preload.ts'], //preload
      },
    }),
  ]
})
```
2. 在 `package.json` 文件中设置scripts
```json
  "scripts": {
    "dev": "vite",
    "build": "vite build",
    "serve": "electron dist/index.js",
  }
```
3. 启动开发服务器
```bash
npm run dev
# or
yarn dev
```
4. 构建项目
```bash
npm run build
# or
yarn build
```

## 注意
1. 如果需要启动 `typescript` 装饰器功能，请设置 `esbuild = false`

## 选项声明
```ts
import { Plugin } from 'vite';
import * as electron_builder from 'electron-builder';
import { RollupCommonJSOptions } from '@rollup/plugin-commonjs';
import { UserOptions as UserOptions$1 } from '@xyh19/rollup-plugin-assets';
import rollup from 'rollup';
import { Options } from 'rollup-plugin-esbuild';
import { CustomTransformerFactories } from '@rollup/plugin-typescript';

interface UserOptions {
    input?: {
        /**
         * app入口
         * @default (__dirname+'../lib/app-entry.js')
         */
        appEntry?: string;
        /**
         * 其他入口(preload等)
         */
        entries?: string | string[];
        srcRoot?: string;
    };
    output?: {
        /**
         * 输出目录
         * @default 'dist'
         */
        dir?: string;
        /**
         * renderer 输出目录，相对于 output.dir
         * @default 'renderer'
         */
        rendererDir?: string;
        /**
         * Whether to Object.freeze() namespace import objects (i.e. import * as namespaceImportObject from...) that are accessed dynamically.
         * @default true
         */
        freeze?: boolean;
        /**
         * 要在捆绑包中添加/附加的字符串。您还可以提供一个函数，该函数返回解析为字符串的承诺，以异步生成它（注意：横幅和页脚选项不会破坏sourcemaps）。
         */
        banner?: string | (() => string | Promise<string>);
        footer?: string | (() => string | Promise<string>);
        intro?: string | (() => string | Promise<string>);
        outro?: string | (() => string | Promise<string>);
        /**
         * 允许创建自定义共享公共块。当使用对象形式时，每个属性代表一个包含列出的模块及其所有依赖项的块，如果它们是模块图的一部分，除非它们已经在另一个手动块中。块的名称将由属性键确定。
         * @example ```
         *  manualChunks: {
         *    lodash: ['lodash']
         *  }
         * ```
         */
        manualChunks?: rollup.ManualChunksOption;
        /** Maps external module IDs to paths. External ids are ids that cannot be resolved or ids explicitly provided by the external option. Paths supplied by output.paths will be used in the generated bundle instead of the module ID, allowing you to, for example, load dependencies from a CDN */
        paths?: rollup.OptionsPaths;
        sourcemap?: boolean | 'inline' | 'hidden';
        sourcemapExcludeSources?: boolean;
        sourcemapFile?: string;
        sourcemapPathTransform?: rollup.SourcemapPathTransformOption;
        validate?: boolean;
    };
    root?: string;
    /**
     * rollup watch
     */
    watch?: rollup.WatcherOptions | false;
    /**
     * 是否强制重启
     * @default false
     */
    forceRestart?: boolean | (() => boolean | Promise<boolean>);
    /**
     * ts配置文件
     * @default (process.cwd()+'tsconfig.json')
     */
    tsconfig?: false | string;
    tsTransformers?: CustomTransformerFactories;
    /**
     * 定义全局常量替换方式。其中每项在开发环境下会被定义在全局，而在构建时被静态替换。
     * @see https://cn.vitejs.dev/config/#define
     */
    define?: Record<string, string>;
    build?: electron_builder.Configuration;
    /**
     * rollup插件
     */
    plugins?: ((rollup.Plugin & {
        enforce?: 'pre' | 'post';
    }) | null | false | undefined)[];
    /**
     * 外部模块
     */
    external?: (string | RegExp)[];
    commonjsOptions?: RollupCommonJSOptions;
    esbuild?: false | Options;
    esbuildOptions?: Options;
    treeshake?: rollup.TreeshakingOptions;
    /**
     * @deprecated
     * 指定额外的 picomatch 模式 作为静态资源处理。
     */
    assetsInclude?: string | RegExp | (string | RegExp)[];
    assetsOptions?: UserOptions$1;
}

declare function electronBuilder(options?: UserOptions): Plugin;

export { UserOptions, electronBuilder as default };
```

## 🤝 Contributing

Contributions, issues and feature requests are welcome!<br />Feel free to check [issues page](https://gitee.com/xxXyh1908/rollup-plugin-assets/issues).

## Give a ⭐️ if this project helped you!